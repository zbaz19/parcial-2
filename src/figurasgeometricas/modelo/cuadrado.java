/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figurasgeometricas.modelo;

import javax.swing.JOptionPane;

/**
 *
 * @author sebit
 */
public class cuadrado {
    private double area;
    private double perimetro;
    public double lado;

    public cuadrado(double area, double perimetro, double lado) {
        this.area = area;
        this.perimetro = perimetro;
        this.lado = lado;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getPerimetro() {
        return perimetro;
    }

    public void setPerimetro(double perimetro) {
        this.perimetro = perimetro;
    }

    public double getLado() {
        return lado;
    }

    public void setLado(double lado) {
        this.lado = lado;
    }

    public static void cuadrado(){
       
            cuadrado[] figura= new cuadrado[5];
            figura[0] = new cuadrado(0, 0, 0);
            figura[1] = new cuadrado(0, 0, 0);
            figura[2] = new cuadrado(0, 0, 0);
            figura[3] = new cuadrado(0, 0, 0);
            figura[4] = new cuadrado(0, 0, 0);
            
            JOptionPane.showMessageDialog(null, figura);
            
}
